# Demo application preview:<br><br>
##### This application allows you to obtain, save and share photos of the Earth that were made by Nasa satelite at different time using NASA official API<br>
## Step 1  - Choose date: 
![step1](screen3.png)
## You can also use search: 
![step2](screen2.png)
## Step 2  - Choose datetime: 
![step3](screen4.png)
## Step 3  - Nasa satellite Earth photo is obtained: 
![step4](screen1.png)
## Step 4  - Share with friends:D
![step5](screen5.png)
