package com.arhiser.nasa_sample.api.model;

public class DateDTO {
    private String date;

    public DateDTO(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
